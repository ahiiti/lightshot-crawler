import os

defaultPath = os.path.join(os.path.expanduser('~'), 'lightshot-crawler')
destinationPath = defaultPath

rawFolder = 'raw'
textFolder = 'text'
categoryFolder = 'categories'

configPath = os.path.abspath('config')
categoryFileDefault = os.path.join(configPath, 'categories.json')
categoryFile = categoryFileDefault

def categoryFilePath():
    return os.path.join(configPath, 'categories.json')

fileNameLength = 2
textExtension = '.txt'


idAlphabet = '0123456789abcdefghijklmnopqrstuvwxyz'


def number_to_id(number, base=len(idAlphabet)):
    """Converts a decimal number to ID"""
    if number < base:
        return idAlphabet[number]

    end = number % base
    return number_to_id(number // base, base=base) + idAlphabet[end]


def id_to_number(id, base=len(idAlphabet)):
    """Converts a ID to decimal number"""
    return int(id, base)


def generate_ids(start, count=None, end=None):
    """Generates image IDs"""
    start = id_to_number(start)
    if end is None:
        end = start + count - 1
    else:
        end = id_to_number(end)

    for num in range(start, end + 1):
        yield number_to_id(num)


def folder_part(id):
    """Returns the id except the last n digits"""
    return id[:len(id) - fileNameLength]


def file_part(id):
    """Returns the last n digits of an ID"""
    return id[len(id) - fileNameLength:]


def join_ext(name, ext):
    """Joins an extension to the file name"""
    return name + '.' + ext.lstrip('.')


def raw_folder_path():
    """Returns the path to the raw folder (for images)"""
    return os.path.join(destinationPath, rawFolder)


def text_folder_path():
    """Returns the path to the text folder (for extracted text)"""
    return os.path.join(destinationPath, textFolder)


def categorized_folder_path():
    """Returns the path to the categorized folder (for categorized images)"""
    return os.path.join(destinationPath, categoryFolder)


def raw_file_path(id, ext):
    """Get the full image path of an ID and extension"""
    # add a dot to the extension if there is none
    full_path = os.path.join(raw_folder_path(), folder_part(id), join_ext(file_part(id), ext))
    return full_path


def text_file_path(id):
    """Get the full text file path of an ID and extension"""
    full_path = os.path.join(text_folder_path(), folder_part(id), file_part(id) + textExtension)
    return full_path


def categorized_file_path(id, ext, category_name, score):
    """Get the full file path of a categorized image with ID and extension"""
    filename = '%s %s' % (str(score).zfill(3), join_ext(id, ext))
    return os.path.join(categorized_folder_path(), category_name, filename)


def get_ids(folder):
    """Returns all IDs and paths ({'id': 'path'}) of folders where the sub-folders
       and file name concatenated produce the image ID"""
    ids = {}
    if not os.path.exists(folder):
        return ids

    for folderpart in os.listdir(folder):
        folderpath = os.path.join(folder, folderpart)
        for filepart in os.listdir(folderpath):
            ids[folderpart + filepart.split('.')[0]] = os.path.join(folderpath, filepart)
    return ids


def get_downloaded():
    """Returns all image IDs and file paths that are downloaded"""
    return get_ids(raw_folder_path())


def get_extracted():
    """Returns all image IDs that are converted to text"""
    return get_ids(text_folder_path())


def create_folder(folder_path):
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)


def create_folder_of_file(file_path):
    create_folder(os.path.dirname(file_path))
