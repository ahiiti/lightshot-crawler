import modules.paths
import threading
import pytesseract

idIterator = None
idIteratorLock = threading.Lock()


def extract(img_id, img_path):
    text = pytesseract.image_to_string(img_path)
    text_file_path = modules.paths.text_file_path(img_id)

    # create folder if not exists
    modules.paths.create_folder_of_file(text_file_path)

    # store extracted text
    with open(text_file_path, 'wb') as file:
        file.write(text.encode('utf-8'))


def next_id():
    """Returns the next id in a thread-safe way
       Returns None when no ids left"""
    idIteratorLock.acquire()
    try:
        img_id = idIterator.__next__()
    except StopIteration:
        img_id = None
    idIteratorLock.release()
    return img_id


def work():
    """Takes ids from next_id() and extracts them until there are none left"""
    while True:
        try:
            id_and_path = next_id()
            if id_and_path is None:
                return
            img_id, img_path = id_and_path

            extract(img_id, img_path)
            print('%s extracted' % img_id)
        except KeyboardInterrupt:
            print('stopping')
            break
        except:
            print('[%s] error' % img_id)


def run(thread_count=1):
    """Runs the extractor"""
    global idIterator
    print('Loading list of downloaded images...')
    downloaded = modules.paths.get_downloaded()
    print('%i downloaded images found.' % len(downloaded))

    print('Loading list of extracted images...')
    extracted = modules.paths.get_extracted()
    print('%i extracted text files found.' % len(extracted))

    print('Generating list of not-yet extracted images...')
    for id in extracted.keys():
        if id in downloaded:
            del downloaded[id]
    print('%i images were not yet extracted.' % len(downloaded))

    idIterator = iter([(key, downloaded[key]) for key in downloaded.keys()])
    print('Now extracting...')

    threads = []
    for i in range(thread_count):
        thread = threading.Thread(target=work)
        thread.daemon = True
        thread.start()
        threads.append(thread)

    # wait for threads to finish or ctrl-c
    try:
        for thread in threads:
            thread.join()
    except KeyboardInterrupt:
        print('\nexiting.')

