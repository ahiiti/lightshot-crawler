import modules.paths
import modules.downloader
import threading
import pytesseract
import json
import shutil
import os


idIterator = None
idIteratorLock = threading.Lock()

categories = None

downloaded = {}


def classify(img_id, text_path):
    text = open(text_path, 'r').read()
    text = text.casefold()



    scores = {}
    for category in categories:
        keywords = categories[category]
        score = 0
        for word in keywords:
            if word in text:
                score += 1
        if score > 0:
            scores[category] = score

    if len(scores) == 0:
        return

    # check if image is downloaded:
    if img_id not in downloaded:
        downloaded[img_id] = modules.downloader.download(img_id)

    # sort images
    for category in scores:
        ext = os.path.splitext(downloaded[img_id])[1]

        dest_file_path = modules.paths.categorized_file_path(img_id, ext, category, scores[category])
        # create folder if not exists
        modules.paths.create_folder_of_file(dest_file_path)
        shutil.copy(downloaded[img_id], dest_file_path)




def next_id():
    """Returns the next id in a thread-safe way
       Returns None when no ids left"""
    idIteratorLock.acquire()
    try:
        img_id = idIterator.__next__()
    except StopIteration:
        img_id = None
    idIteratorLock.release()
    return img_id


def work():
    """Takes ids from next_id() and sorts them until there are none left"""
    while True:
        try:
            img_id = next_id()
            if img_id is None:
                return
            classify(img_id[0], img_id[1])
            print('[%s] classified' % img_id[0])
        except KeyboardInterrupt:
            print('stopping')
            break


def run(thread_count=1):
    """Runs the classifier"""
    global idIterator, categories, downloaded

    print('Loading categories...')
    with open(modules.paths.categoryFilePath(), 'r') as file:
        categories = json.load(file)
    print('Loaded %i categories: %s' % (len(categories.keys()), ', '.join(categories.keys())))

    print('Loading list of extracted images...')
    extracted = modules.paths.get_extracted()
    print('%i extracted text files found.' % len(extracted))

    print('Loading list of downloaded images...')
    downloaded = modules.paths.get_downloaded()
    print('%i downloaded imgages foud')

    idIterator = iter([(key, extracted[key]) for key in extracted.keys()])
    print('Now categorizing...')

    threads = []
    for i in range(thread_count):
        thread = threading.Thread(target=work)
        thread.daemon = True
        thread.start()
        threads.append(thread)

    # wait for threads to finish or ctrl-c
    try:
        for thread in threads:
            thread.join()
    except KeyboardInterrupt:
        print('\nexiting.')

