DISCLAIMER
THIS PROGRAM IS INTENDED FOR EDUCATIONAL AND DEMONSTRATIONAL USE ONLY.
EVERY USER IS RESPONSIBLE FOR THEIR OWN ACTIONS.


# lightshot-crawler
`lightshot-crawler` is a proof-of-concept tool to demonstrate the exploitability of the [Lightshot screenshot sharing tool](https://app.prntscr.com).
## Background
Screenshots shared on Lightshot are available via alphanumerical IDs, e.g `https://prnt.sc/123abc`.
The ID gets incremented for every image. This makes iterating over images easy.
(e.g. `123456` -> `123457`, `blobby` -> `blobbz`)

Additionally, some users don't care about sharing confidential information via publicly available screenshots.
This is an invitation to us all to have some fun with it.

This program automatically downloads screenshots for further processing. A built-in text recognition and content scoring feature might come in the future.


*Free screenshots for everyone!*  
*Have fun!*

## Dependencies
`lightshot-crawler` needs the `tesseract` library and the `pytesseract` python module, and `python3`, of course.

### Fedora
    dnf install python3 tesseract
    pip3 install pytesseract

### Ubuntu
    apt install python3 python3-pip tesseract-ocr
    pip3 install setuptools
    pip3 install wheel
    pip3 install pytesseract


## The Program
...consists of 3 modules:


### `download` module
Downloads screenshots.

Example:

    python3 lightshot-crawler.py download --start l00000 --count 500 --threads 4 --destination ~/lightshot-crawler

This will download 500 images, starting with the ID `l00000` to
`~/lightshot-crawler` using 4 simultaneous threads.

More options / help:

    python3 lightshot-crawler.py download -h

### `extract` module
Runs text recognition over downloaded screenshots and stores the results
in text files.

Uses all CPU cores by default.

Example:

    python3 lightshot-crawler.py extract --destination ~/lightshot-crawler --threads 2

This will recognize the text in images and store the results as .txt
files

More options / help:

    python3 lightshot-crawler.py extract -h

### `classify` module (in development)
Gives each image a score for each category defined in `config/categories.json` and copies them in the `categories` subfolder.

Example:
    python3 lightshot-crawler.py classify

This will run the classifyer over all extracted images.
