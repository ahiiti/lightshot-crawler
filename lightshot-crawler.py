#!/bin/python3

try:
    import modules.downloader
except ModuleNotFoundError:
    pass

try:
    import modules.extractor
except ModuleNotFoundError:
    pass

try:
    import modules.classifier
except ModuleNotFoundError:
    pass

try:
    import modules.paths
except ModuleNotFoundError:
    pass

import argparse
import multiprocessing


def download():
    """runs the downloader module using the parsed arguments"""
    print('Running downloader.')
    modules.downloader.run(args.start, end_id=args.end, count=args.count, thread_count=args.threads)


def extract():
    """runs the extractor module using the parsed arguments"""
    print('Running extractor')
    modules.extractor.run(thread_count=args.threads)

def classify():
    """runs the classifier using the parsed arguments"""
    print('Running classifier')
    modules.paths.categoryFile = args.categoryFile
    modules.classifier.run()


parser = argparse.ArgumentParser(description='Download and alalyze screenshots from lightshot.')
parser.set_defaults(func=parser.print_help)
subparsers = parser.add_subparsers(title='commands')

parser_download = subparsers.add_parser('download', help='Downloads screenshots')
parser_download.set_defaults(func=download)
parser_download.add_argument('--start', type=str, help='ID of the first image to download', required=True)
parser_download.add_argument('--end', type=str, help='ID of the last image to download')
parser_download.add_argument('--count', type=int, help='Number of images to download', default=1)
parser_download.add_argument('--destination', type=str, help='Path to the working directory',
                             default=modules.paths.defaultPath)
parser_download.add_argument('--threads', type=int, help='Number of worker threads', default=1)


parser_extract = subparsers.add_parser('extract', help='Recognizes all text from the images and stores it in text files.')
parser_extract.set_defaults(func=extract)
parser_extract.add_argument('--destination', type=str, help='Path to the working directory',
                             default=modules.paths.defaultPath)
parser_extract.add_argument('--threads', type=int, help='Number of worker threads', default=multiprocessing.cpu_count())


parser_classify = subparsers.add_parser('classify', help='Classifies screenshots using extracted text.')
parser_classify.set_defaults(func=classify)
parser_classify.add_argument('--destination', type=str, help='Path to the working directory',
                             default=modules.paths.defaultPath)
parser_classify.add_argument('--categoryFile', type=str, help='Path of the file specifying categories')

# parse args
args = parser.parse_args()

# apply global settings
modules.paths.destinationPath = args.destination

# run sub-commands
args.func()
